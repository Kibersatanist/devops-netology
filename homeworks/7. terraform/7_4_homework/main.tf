provider "aws" {
  profile = "terraform"
  region = "eu-central-1"
  access_key = "AKIAQM7J2T2UR72NFQWI"
  secret_key = "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"
}

data "aws_ami" "ubuntu-free" {
  most_recent = true
  owners = ["099720109477"]

  filter {
	name = "name"
	values = ["*ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
	name = "virtualization-type"
	values = ["hvm"]
  }
}

module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "my-cluster"
  instance_count         = 1

  ami                    = data.aws_ami.ubuntu-free.id
  instance_type          = "t2.micro"
  monitoring             = true
  vpc_security_group_ids = ["sg-c1230ab1"]
  subnet_id = "subnet-72235f18"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}