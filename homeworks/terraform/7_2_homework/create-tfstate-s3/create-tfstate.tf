provider "aws" {
    profile = "terraform"
    region = "eu-central-1"
}

terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"
        }
    }
}

// Создаём ресурс для хранения tfstate
resource "aws_s3_bucket" "terraform-state" {
  bucket = "kibersatanist-terraform-state"
  acl = "private"

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "terraform-locks" {
  hash_key = "LockID"
  name = "terraform-locks"
  billing_mode = "PAY_PER_REQUEST"
  attribute {
    name = "LockID"
    type = "S"
  }
}