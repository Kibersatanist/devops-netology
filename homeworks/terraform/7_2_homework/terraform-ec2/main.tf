provider "aws" {
  profile = "terraform"
  region = "eu-central-1"
  access_key = "AKIAQM7J2T2UR72NFQWI"
  secret_key = "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"
}

locals {
  web_instance_type_map = {
	stage = "t2.micro"
	prod = "t2.large"
  }
}

locals {
  web_instance_count = {
	stage = 1
	prod = 2
  }
}

data "aws_ami" "ubuntu-free" {
  most_recent = true
  owners = ["099720109477"]

  filter {
	name = "name"
	values = ["*ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
	name = "virtualization-type"
	values = ["hvm"]
  }
}
data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

resource "ec2_cluster" "web" {
  ami = data.aws_ami.ubuntu-free.id
  instance_type = "t2.micro"
  hibernation = false
  monitoring = true
  count = 1
  security_groups = ["default"]

  lifecycle {
	create_before_destroy = true
  }
}