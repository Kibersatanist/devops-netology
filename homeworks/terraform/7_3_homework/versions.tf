terraform {
  required_providers {
    aws = {
      source = "hasicorp/aws"
      version = "~> 3.0"
    }
  }
}