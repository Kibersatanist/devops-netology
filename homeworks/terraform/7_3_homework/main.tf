provider "aws" {
  region = "eu-east-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "test-bucket-netology-1"
  acl = "private"
}