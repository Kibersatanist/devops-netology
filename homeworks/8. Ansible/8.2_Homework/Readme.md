# ELK-Stack playbook

Плейбук разделён на 3 основные секции
* Установка и настройка Java (тег: java)
* Установка и настройка ElasticSearch (тег: elasticsearch)
* Установка и настройка Kibana (тег: kibana)

Плейбук тестировался на ubuntu-20.04, параметры хоста: VPS 6 core / 6 RAM / 24 SSD

## Конфиги
Конфиги лежат в папке `configs`
* `elasticsearch.service.j2` — systemd-unit для elasticsearch (т.к устанавливается не через apt)
* `elasticseatch.yml.j2` — основной конфигурационный файл elasticsearch
* `jvm.options` — файл с опциями java, с которыми запускается elasticsearch
* `kibana.uml.j2` — основной файл конфигурации kibana

## Group vars
В `group_vars/elasticsearch.yml` вы управляете версией устанавливаемого elasticsearch и путём установки. 
**Внимание! Kibana будет установлена соответствующей версии (такой-же как и elasticsearch) во избежание несовместимости версий**

В `group_vars/all/vars.yml` вы можете указать путь до архива с JDK (который предварительно надо скачать с официального сайта)

## Прочие файлы
`./templates` — содержит в себе файлы, необходимые для внесения путей до установленных из исходников программ 
(elasticsearch и java) в PATH