#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
import os
from ansible.module_utils._text import to_bytes

DOCUMENTATION = r'''
---
module: my_test

short_description: The module creates a file at the specified path with the specified content

version_added: "1.0.0"

description: The module creates a file at the specified path with the specified content.

options:
    name:
        description: The name of the file to be created
        required: true
        type: str
    path:
        description: Path of the file with `name`
        required: true
        type: str
    content:
        description: Content of the created file
        required: true
        type: str
    force: 
        description: Force recreate file flag
        required: true
        default: False
        type: str

extends_documentation_fragment:
    - my_namespace.my_collection.my_doc_fragment_name

author:
    - Saimon Vasilinin (@kibersatanist)
'''

EXAMPLES = r'''
- my_own_collection.my_own_module:
        content: successful!
        force: false
        name: test.txt
        path: /tmp/testfolder/
      name: "run my_own_module"
      register: output
    - debug:
      msg: "{{ output }}"
      name: "dump output"
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule

def run_module():
    # Определяем аргументы, которые принимает модуль
    module_args = dict(
        name=dict(type='str', required=True),
        path=dict(type='str', required=True),
        content=dict(type='str', required=True),
        force=dict(type=bool, required=False, default=False)
    )

    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # Проверка на случай, если забыли слеш указать
    if module.params['path'][:-1] == '/':
        generate_path = module.params['path'] + module.params['name']
    else:
        generate_path = module.params['path'] + '/' + module.params['name']
    # Проверяем имеется ли по данному пути директория и права на запись в неё
    need_create = os.access(generate_path, os.F_OK) and not module.params['force']
    if module.check_mode or need_create:
        result['original_message'] = "File exists"
        result['message'] = "File already exists. If you want to replace existing files – you need to set Force param to True"
        module.exit_json(**result)
    os.makedirs(module.params['path'], exist_ok=True)
    with open(generate_path, 'wb') as newone:
        newone.write(to_bytes(module.params['content']))
    result['original_message'] = "successful created"
    result['message'] = "goodbye"
    result['changed'] = True
    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()