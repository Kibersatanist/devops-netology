terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.61.0"
    }
  }
}

variable "token" {
  description = "Ya Cloud secret token"
  type        = string
  sensitive   = true
}

provider "yandex" {
  token     = var.token
  cloud_id  = "b1gqt7hkqj78gfke93tu"
  folder_id = "b1g6oeccm6gu3bs4n198"
}

variable "ru-central1-a" {
  type    = string
  default = "e9bhriu9kpsc4ug09b65"
}

resource "yandex_compute_instance" "testserver" {
  zone = "ru-central1-a"
  name = "testserver"

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      name        = "testserver"
      description = "Диск для сервера на котором я обучаюсь"
      type        = "network-ssd"
      size        = 16
      image_id    = "fd83klic6c8gfgi40urb"
    }
  }

  network_interface {
    subnet_id      = var.ru-central1-a
    nat_ip_address = "178.154.206.19"
    nat            = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }
}